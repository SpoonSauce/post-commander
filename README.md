# post-commander
<img src="icon.png" width="285px" height="285px"/><br/>
POST Commander executes shell commands based on HTTP POST requests.

*This code is intended for experimentation only and is not a complete software package.*

## Basic Usage
The application can be run without installing using `./post-commander.py <port>`, where `<port>` is the TCP port to bind the HTTP server to (e.g., `80` or `12345`).  If no port is specified, POST Commander will default to using `8081`.

## Installation
Create user systemd directory if it doesn't already exist:
```
mkdir -p ~/.config/systemd/user/
```

Copy and set permissions for the systemd unit:
```
cp ./post-commander.service.template ~/.config/systemd/user/post-commander.service
chmod 644 ~/.config/systemd/user/post-commander.service
```

Edit the working directory of POST Commander in the unit file:
```
vim ~/.config/systemd/user/post-commander.service
```

Change `WorkingDirectory=` to the correct path:
```
WorkingDirectory=/path/to/post-commander/
```

Optionally, the TCP port binding can also be changed here:
```
ExecStart=/usr/bin/python post-commander.py <port>
```

Reload systemd so it detects our new service:
```
systemctl --user daemon-reload
```

Enable and start the service:
```
systemctl --user enable post-commander.service
systemctl --user start post-commander.service
```

## Built-in Commands
A few built-in commands are available.  Here are a few requests using cURL.

### Run/Quit Gedit
```
curl -d "cmd=start" http://localhost:8081/gedit/
curl -d "cmd=stop" http://localhost:8081/gedit/
```

### Run/Quit Google Play Music Desktop Player
```
curl -d "cmd=start" http://localhost:8081/google_play_music/
curl -d "cmd=stop" http://localhost:8081/google_play_music/
```

### Run/Quit Kodi
```
curl -d "cmd=start" http://localhost:8081/kodi/
curl -d "cmd=stop" http://localhost:8081/kodi/
```

### Run/Quit Moonlight
```
curl -d "cmd=start" http://localhost:8081/moonlight/
curl -d "cmd=stop" http://localhost:8081/moonlight/
```

### Shutdown the System
```
curl -d "cmd=start" http://localhost:8081/shutdown/
```

### Restart the System
```
curl -d "cmd=start" http://localhost:8081/restart/
```

## Adding Commands
New commands can be added in the following section of `post-commander.py`:
```python
### BEGIN COMMAND DEFINITIONS ###

# Gedit
if path == 'gedit':
   # Start
   if cmd in startCMDs:
      pid = runCommand("gedit")
      pidList[path].append(pid)
   # Stop
   elif cmd in stopCMDs:
      killProcess(pidList[path])
      pidList[path] = []
   # Custom command
      #elif cmd == "mycommand":
      #    pid = runCommand("dothis")
      #    # Optional - Add only if you want the PID to be
      #    # killed with all other associated PIDs
      #    pidList[path].append(pid)

#
# ...
#

### END COMMAND DEFINITIONS ###
```

*Restart `post-commander.service` after making any changes.*

## Removal
Stop and disable the service:
```
systemctl --user stop post-commander.service
systemctl --user disable post-commander.service
```

Delete the systemd unit:
```
rm ~/.config/systemd/user/post-commander.service
```

Delete log files:
```
rm -r ~/.post-commander/
```

## Additional Credits

Project icon made by [photo3idea_studio](https://www.flaticon.com/authors/photo3idea-studio) from [www.flaticon.com](www.flaticon.com).
