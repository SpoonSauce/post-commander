#!/usr/bin/env python

# Simple Python HTTP server.
# Based on https://gist.github.com/bradmontgomery/2219997

# Usage:
#
#   ./httpd.py <port>

# Example cURL calls:
#
#   Send a GET request:
#   curl http://localhost:<port>
#
#   Send a HEAD request:
#   curl -I http://localhost:<port>
#
#   Send a POST request with data:
#   curl -d "foo=bar&bin=baz" http://localhost:<port>
#
#   Send a POST file attachment:
#   curl -F "filename.ext=@/path/to/filename.ext" http://localhost:<port>
#
#   Send a POST file attachment with data:
#   curl -F "filename.ext=@/path/to/filename.ext" -F "foo=bar" -F "bin=baz" http://localhost:<port>

# Imports
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import cgi

# Request handler
class S(BaseHTTPRequestHandler):
    # Set headers
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    # Handle GET request
    def do_GET(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>Hello World!</h1></body></html>")

    # Handle HEAD request
    def do_HEAD(self):
        self._set_headers()

    # Handle POST request
    def do_POST(self):
        # Parse the form data posted
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })

        # Begin the response
        self.send_response(200)
        self.end_headers()
        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Form data:\n')

        # Echo back information about what was posted in the form
        for field in form.keys():
            if form[field].filename:
                # The field contains an uploaded file
                file_data = form[field].file.read()
                file_len = len(file_data)

                # Delete the data
                del file_data

                # Output to client
                self.wfile.write("\r\n")
                self.wfile.write('\tUploaded %s as "%s" (%d bytes)\n' % \
                    (field, form[field].filename, file_len))
            else:
                # Output to client
                self.wfile.write("\r\n")
                self.wfile.write("\tform[field]: " + str(form[field]) + "\r\n")
                self.wfile.write("\tfield: " + str(field) + "\r\n")
                self.wfile.write("\tform[field].value: " + str(form[field].value) + "\r\n")

        # Return
        return

# Server run function
def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

# Main process
if __name__ == "__main__":
    from sys import argv

    # Port specified in arguments
    if len(argv) == 2:
        run(port=int(argv[1]))
    # No port specified, run on port 80
    else:
        run()
