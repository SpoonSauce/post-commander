#!/usr/bin/env python

# POST Commander executes shell commands based on HTTP POST requests.

# Usage:
#
#   ./post-commander.py <port>

# Example cURL calls:
#
#   Send a POST request to start Gedit:
#   curl -d "cmd=start" http://localhost:<port>/gedit/

### IMPORTS ###

# OS for directory validation and creation and output redirection
import os

# Path expander to get proper home directory
from os.path import expanduser

# Subprocess for running commands
import subprocess

# Web server imports
from http.server import BaseHTTPRequestHandler, HTTPServer
import socketserver
import cgi

# Time for sleep functions
import time

# Logging for log file handling
import logging
from logging.handlers import RotatingFileHandler

### CONFIG AND LOGGING ###

# Potential start/stop commands
startCMDs = ["run","start","exec"]
stopCMDs = ["quit","stop","exit"]

# OS-independent null path
DEVNULL = open(os.devnull, 'r+b', 0)

# Create log path if it does not already exist
homeDir = expanduser("~")
logPath = homeDir+'/.post-commander/logs/'
if not os.path.exists(logPath):
	os.makedirs(logPath)

# Thread for server logs
serverLogs = logging.getLogger('httpd-server-logs')
serverLogs.setLevel(logging.DEBUG)

handler = RotatingFileHandler(logPath+'post-commander.log', maxBytes=500*1000, backupCount=5)
logFormat = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')
handler.setFormatter(logFormat)
serverLogs.addHandler(handler)

### CLASSES ###

# Request handler
class S(BaseHTTPRequestHandler):
	# Set headers
	def _set_headers(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()

	# Handle GET request
	def do_GET(self):
		# Blank output
		self._set_headers()
		self.wfile.write("")

	# Handle HEAD request
	def do_HEAD(self):
		self._set_headers()

	# Handle POST request
	def do_POST(self):
		# Parse the form data posted
		form = cgi.FieldStorage(
			fp=self.rfile,
			headers=self.headers,
			environ={'REQUEST_METHOD':'POST', 'CONTENT_TYPE':self.headers['Content-Type'], })

		# Begin the response
		self.send_response(200)
		self.end_headers()

		# Handle form data
		for field in list(form.keys()):
			if form[field].filename:
				# The field contains an uploaded file
				file_data = form[field].file.read()

				# Delete the data as we have no use for it
				del file_data
			else:
				# Ensure we've specified a path
				if self.path is None or self.path == "":
					serverLogs.warn("["+str(self.client_address[0])+"] did not specify a path!")
				# Get path without leading or trailing slashes
				else:
					path = self.path.replace('/','')

				# Ensure we've specified a command
				if field is None or field != "cmd":
					serverLogs.warn("Client at "+str(self.client_address)+" did not specify a command!")
				# Get command
				else:
					cmd = form[field].value

				# We have a command and a path so we can proceed
				if path is not None and cmd is not None:
					# Log
					serverLogs.info("["+str(self.client_address[0])+"] requested ["+cmd+"] command for ["+path+"]")

					# In this section we define the dictionary which will store
					# process ID (PID) lists for processes started with this
					# script.

					# Reset pid
					pid = False

					# Create out pidList dictionary if it doesn't exist
					global pidList
					try:
						pidList
					except NameError:
						pidList = {}

					# Create a list for this path/application if it doesn't exist
					try:
						pidList[path]
					except KeyError:
						pidList[path] = []

					# In this section we define commands which will run when
					# the correct path / command combination is requested.
					#
					# This is what you'll want to edit to add additional
					# commands.

					### BEGIN COMMAND DEFINITIONS ###

					# Gedit (Example)
					if path == 'gedit':
						# Start
						if cmd in startCMDs:
							pid = runCommand("gedit")
							pidList[path].append(pid)
						# Stop
						elif cmd in stopCMDs:
							killProcess(pidList[path])
							pidList[path] = []
						# Custom command
						#elif cmd == "mycommand":
						#	pid = runCommand("dothis")
						#	# Optional - Add only if you want the PID to be
						#	# killed with all other associated PIDs
						#	pidList[path].append(pid)

					# Google Play Music Desktop Player
					elif path == 'google_play_music':
						# Start
						if cmd in startCMDs:
							pid = runCommand("google-play-music-desktop-player")
							pidList[path].append(pid)
						# Stop
						elif cmd in stopCMDs:
							killProcess(pidList[path])
							pidList[path] = []

					# Kodi
					elif path == 'kodi':
						# Start
						if cmd in startCMDs:
							pid = runCommand("kodi")
							pidList[path].append(pid)

							# Forking process: Wait for secondary process to start...
							# Kodi forks into either kodi-x11 or kodi-wayland depending on the compositor being used
							serverLogs.info("Waiting for secondary process to start...")
							spid = False
							while not spid:
								# Try kodi-x11
								spid = "pidof -s kodi-x11"
								spid = subprocess.Popen(spid, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL)
								spid = spid.communicate()[0]
								spid = spid.strip()

								# If we have a PID break immediately
								if spid:
									break

								# Otherwise wait...
								time.sleep(0.5)

								# Try kodi-wayland
								spid = "pidof -s kodi-wayland"
								spid = subprocess.Popen(spid, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL)
								spid = spid.communicate()[0]
								spid = spid.strip()

								# Wait...
								time.sleep(0.5)

							# Save additional PID
							pidList[path].append(spid)
							serverLogs.info("New process: PID ["+str(spid.decode())+"]")
						# Stop
						elif cmd in stopCMDs:
							killProcess(pidList[path])
							pidList[path] = []

					# Moonlight (Flatpak)
					elif path == 'moonlight':
						# Start
						if cmd in startCMDs:
							pid = runCommand("flatpak run com.moonlight_stream.Moonlight")
							pidList[path].append(pid)

							# Forking process: Wait for secondary process to start...
							serverLogs.info("Waiting for secondary process to start...")
							spid = False
							while not spid:
								spid = "pidof -s moonlight"
								spid = subprocess.Popen(spid, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL)
								spid = spid.communicate()[0]
								spid = spid.strip()

								# Wait...
								time.sleep(0.5)

							# Save additional PID
							pidList[path].append(spid)
							serverLogs.info("New process: PID ["+str(spid.decode())+"]")
						# Stop
						elif cmd in stopCMDs:
							killProcess(pidList[path])
							pidList[path] = []

					# Shutdown or Reboot System
					elif path == 'shutdown' or path == 'reboot' or path == 'restart':
						# Execute
						if cmd in startCMDs:
							# Log shutdown/reboot attempt
							serverLogs.warn("The system will "+path+" in 5 seconds...")

							# Wait...
							time.sleep(5)

							# Attempt shutdown/reboot
							if path == 'shutdown':
								pid = runCommand("shutdown -h now 'POST Commander requested a system shutdown.'")
							else:
								pid = runCommand("shutdown -r now 'POST Commander requested a system reboot.'")

							# If we have no pid, the request likely failed
							if not pid:
								serverLogs.error("Unable to "+path+" system! This may be due to a permission issue.")
							else:
								serverLogs.info("System going down for "+path+"!.")

					### END COMMAND DEFINITIONS ###

					# Bad process specified
					else:
						serverLogs.warn("Invalid path: ["+path+"]")

					# If we have a pid our process was created
					if pid:
						serverLogs.info("New process: PID ["+str(pid.decode())+"]")
					# Bad command specified
					elif cmd not in stopCMDs:
						serverLogs.warn("Invalid command: ["+cmd+"]")

		#/for field in form.keys()""

		# Return
		return

	#/def do_POST():

### FUNCTIONS ###

# Command execution function
def runCommand(myCommand):
	# Try starting new process
	try:
		# Log command and start new process
		serverLogs.info("Executing: "+myCommand)
		process = subprocess.Popen(myCommand, shell=True, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL)

		# Return our new process ID
		return process.pid
	# Log OS error
	except OSError:
		serverLogs.error("An error occurred executing the command!")
	# Other error
	except:
		serverLogs.error("An unexpected error has occurred!")

# Check for the existence of a process by PID
def checkProcess(pid):	
	# Kill 0 will simply confirm whether ot not the process exists	
	try:
		os.kill(pid,0)
	# OSError means our process is not running
	except OSError:
		return False
	# Otherwise process is running
	else:
		return True

# Kill processes by PID list
def killProcess(pidList):
	# Define kill levels in order they will be used
	# 15 = SIGTERM
	#  2 = SIGINT
	#  1 = SIGHUP
	#  3 = SIGQUIT
	#  9 = SIGKILL
	levels = [15,2,1,3,9]

	# Make sure we have at least one PID
	if pidList != []:
		# Kill each pid in reversed pidList
		for pid in reversed(pidList):
			# Loop until process is dead and gone
			running = checkProcess(int(pid))
			while running:
				# Wait and check if process has quit
				for level in levels:
					# Run kill command
					runCommand("kill -"+str(level)+" "+str(pid.decode()))

					# Wait...
					time.sleep(5)

					# Check if process is still running
					running = checkProcess(int(pid))

					# Log once process stops running
					if not running:
						serverLogs.info("Killed: PID ["+str(pid.decode())+"]")
						break
				# After SIGKILL we assume the process has stopped
				running = False
				serverLogs.warn("Reached SIGKILL: PID ["+str(pid.decode())+"]")
		# Done!
		serverLogs.info("All process IDs killed.")
		return True
	# No PIDs
	else:
		# Log
		serverLogs.warn("No process IDs found to kill!")
		return False

# Server run function
def run(server_class=HTTPServer, handler_class=S, port=8081):
	server_address = ('', port)
	httpd = server_class(server_address, handler_class)
	serverLogs.info('Starting POST Commander HTTP daemon...')
	httpd.serve_forever()

### MAIN PROCESS ###

if __name__ == "__main__":
	from sys import argv

	# Port specified in arguments
	if len(argv) == 2:
		run(port=int(argv[1]))
	# No port specified, run on port 80
	else:
		run()
